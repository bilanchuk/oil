<?php
/**
 * @var \App\Material $materials
 */
?>
@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content shop-grid">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">{{ __('Home') }}</a>
                            </li>
                            <li class="active">
                                <a href="{{ url('marketing_materials') }}">Marketing Materials</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->

            <div class="row">
                <div class="col-md-12">
                    <div class="product-listing-view">
                        <div class="view-navigation">
                            <div class="info-text">
                                <p>{{ __(trans_choice('{1} Showing :num material|[1,*] Showing 1-:num from :num materials', count($page->materials ?? $materials)), ['num' => count($page->materials ?? $materials)]) }}</p>
                            </div>
                            <div class="right-content">
                                <div class="grid-list">
                                </div>
                                <div class="input-select">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="shop-product-list">
                                @each('components.materials', $page->materials ?? $materials ?? [], 'material', 'components.empty-material')
                            </div>
                        </div>
                        <div class="content">
                            {!! $page->body ?? null !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--/.container-->
    </div>
    <!--/.about-content-->


<script src="{{ asset('js/sweetalert2.min.js') }}?{{ time() }}"></script>
<link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}?{{ time() }}">


<script>

    function order(item_name, minOrder) {

        swal.setDefaults({
            input: 'text',
            confirmButtonText: 'Next &rarr;',
            confirmButtonColor: '#202020',
            showCancelButton: true,
            animation: false,
            progressSteps: ['1', '2', '3', '4']
        });

        var steps = [
            {
                title: 'Amount (min. ' + minOrder + ' items)',
                text: 'To complete your order we need you to specify the amount of items you need to order, your full name, email and contact phone. Let\'s start with an amount',
                input: 'number',
                inputValue: minOrder,
                inputPlaceholder: 'Amount',
                inputValidator: function (value) {
                    return new Promise(function (resolve, reject) {
                        if (value !== '') {
                            if (value < minOrder) {
                                reject('Sorry, the minimum order is amount ' + minOrder)
                            } else {
                                resolve()
                            }
                        } else {
                            reject('Sorry, you can\'t leave it blank. We need your name to complete the order')
                        }
                    })
                }
            },
            {
                title: 'Full Name',
                text: 'Now, enter your Full Name',
                input: 'text',
                inputPlaceholder: 'Full Name',
                inputValidator: function (value) {
                    return new Promise(function (resolve, reject) {
                        if (value !== '') {
                            resolve()
                        } else {
                            reject('Sorry, you can\'t leave it blank. We need your full name to complete the order.')
                        }
                    })
                }
            },
            {
                title: 'Email',
                text: 'We will use your email only for sending information related to this order',
                input: 'email',
                inputPlaceholder: 'Email Address',
                inputValidator: function (email) {
                    return new Promise(function (resolve, reject) {
                        if (email !== '') {
                            var emailRegex = /^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
                            if (emailRegex.test(email)) {
                                resolve();
                            } else {
                                reject('Email address you entered is invalid, please, check it end try again.');
                            }
                        } else {
                            reject('Sorry, you can\'t leave it blank. We need your email to complete the order')
                        }
                    })
                }
            },
            {
                title: 'Phone Number',
                text: 'We need your phone number to contact you after the purchase',
                input: 'text',
                inputPlaceholder: 'Phone Number',
                inputValidator: function (value) {
                    return new Promise(function (resolve, reject) {
                        if (value !== '') {
                            resolve()
                        } else {
                            reject('Sorry, you can\'t leave it blank. We need your phone number to be able to contact you after the order is finished')
                        }
                    })
                }
            }
        ];

        swal.queue(steps).then(function (result) {
            swal.resetDefaults();

            $.post("{{ route('contact_attempts.order_marketing_materials') }}", {
                '_token': '{{ csrf_token() }}',
                'title': 'New order on petromericaoil.com: ' + item_name,
                'from': 'order@petromericaoil.com',
                'from_name': 'Order Bot',
                'name': result[1],
                'email': result[2],
                'phone': result[3],
                'text': 'Order details: ' + 'ordered item - ' + item_name + ' amount - ' + result[0]
            });

            swal({
                title: "<i class=\"fa fa-check-circle\" aria-hidden=\"true\"></i> All Done!",
                html: "Your order has been successfully added to our system.",
                type: "success",
                confirmButtonColor: '#202020',
                animation: "slide-from-top"
            });
        }, function () {
            swal.resetDefaults();
        });
    }

</script>
@endsection
