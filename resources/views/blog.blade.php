@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="active"><a href="{{ url('/blog') }}">Blog</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->

            <!-- content -->
            <div class="content blog-page">
                <div class="row">
                    <div class="col-md-9 col-sm-12">
                        <div class="blog_content clearfix">
                            @each('components.blog-listing-item', \App\Article::all(), 'article')
                        </div>
                        <!-- end of blog_content -->

                        {{--<div class="paginations">
                            <ul class="pagination">
                                <li><a href="#"><span class="pagicon arrow_carrot-left"></span></a>
                                </li>
                                <li><a href="#"><span>1</span></a>
                                </li>
                                <li class="active"><a href="#"><span>2</span></a>
                                </li>
                                <li><a href="#"><span>3</span></a>
                                </li>
                                <li><a href="#"><span>4</span></a>
                                </li>
                                <li><a href="#"><span>5</span></a>
                                </li>
                                <li><a href="#"><span>6</span></a>
                                </li>
                                <li><a href="#"><span class="pagicon arrow_carrot-right"></span></a>
                                </li>
                            </ul>
                        </div>--}}
                    </div>
                    <!-- end of col-md-9 -->

                    <!-- sidebar -->
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="sidebar">
                            <div class="widget widget-recent-post">
                                <h3 class="widget-title">RECENT POSTS</h3>
                                <div class="recent_post">
                                    @each('components.blog-recent-posts', \App\Article::all(), 'article')
                                </div>
                            </div>
                            <!-- end of sidebar_widget -->
                        </div>
                        <!-- end of sidebar -->
                    </div>
                    <!-- end of col-md-3 -->
                </div>
                <!-- end of row-->
            </div>
            <!--/ content -->

        </div>
        <!--/.container-->
    </div>
    <!--/.about-content-->

@endsection
