@extends('layouts.master')

@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="active"><a href="#">search result</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->

            <div class="row">
                <div class="col-md-12">
                    <div class="product-listing-view">
                        <div class="row">
                            <div class="shop-product-list shop-list">
                                <div class="col-md-12">
                                    @foreach([1, 2, 3, 4] as $product)
                                    <!--/.product-single-->
                                    <div class="product-single">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="product-info">
                                                <h2>Lorem ipsum</h2>
                                                <div class="entry-content">
                                                    <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                                                </div>
                                            </div>
                                            <!--/.product-info-->
                                        </div>
                                    </div>
                                    <!--/.product-single-->
                                    @endforeach
                                </div>
                                <!--/.col-md-12-->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--/.container-->
    </div>
    <!--/.about-content-->
@endsection