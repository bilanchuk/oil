<?php
/**
 * @var \App\Article $article
 */
?>
<div class="col-md-4">
    <article class="post">
        <div class="post-thumb-content">
            <figure class="post-thumb">
                <a href="{{ $article->link }}">
                    <img class="img-responsive" alt="thumb" src="{{ Voyager::image($article->image) }}">
                </a>
            </figure>
            <!--/.post thumb-->
            <span class="entry-date">{{ $article->created_at->format('jS F Y') }}</span>
        </div>
        <!--/.post-thumb-content-->
        <div class="post-details">
            <h3 class="entry-title">
                <a href="{{ $article->link }}">{{ $article->title }}</a>
            </h3>
            <!--/.entry title-->
            <!--/.meta-->
            <div class="entry-content">
                <p>{!! $article->excerpt !!}<a class="read-more pull-right" href="{{ $article->link }}">READ MORE</a></p>
            </div>
            <!--/.entry content-->
        </div>
        <!--/.post details-->
    </article>
    <!--/.post-->
</div>
<!--/.col-md-6-->
