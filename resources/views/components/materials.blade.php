<div class="col-md-4 col-sm-6 col-xs-6 product_item material_item">
    <div class="product-single material-single">
        <div class="product-thumb material-thumb">
            @if ($material->image)
                <img class="img-responsive" alt="Single material"
                        src="{{ Voyager::image($material->thumbnail('small')) }}">
            @endif
        </div>
        <!--/.product-thumb-->
        <div class="product-info material-thumb">
            <h2 class="price"> {{ $material->title }} </h2>
            <div class="material-bottom">
                <div class="material-info">
                    <p>Price: ${{ $material->price }}</p>
                    <p>Min. Order: {{ $material->min_order }}</p>
                </div>
                <div class="material-buy">
                    <a class="material-order" href="javascript:order('{{ $material->title }}', {{ $material->min_order }})">Order</a>
                </div>
            </div>
        </div>
        <!--/.product-info-->
    </div>
    <!--/.product-single-->
</div>
<!--/.col-md-4-->
