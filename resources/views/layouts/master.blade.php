<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158765108-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-158765108-1');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="keywords" content="@yield('meta_keyword')">
    <meta name="description" content="@yield('meta_description')">
    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/eleganticon.css">
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="/rs-plugin/css/style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/rs-plugin/css/navstylechange.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/rs-plugin/css/noneed.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/rs-plugin/css/settings.css" media="screen" />
    <!-- MAGNIFIC POPUP -->
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <!-- OWL CAROUSEL -->
    <link rel="stylesheet" href="/css/owl.carousel.css">
    <!-- ANIMATE CSS -->
    <link rel="stylesheet" href="/css/animate.css">
    <!-- FANCYBOX CSS -->
    <link rel="stylesheet" href="/css/jquery.fancybox.min.css">
    <!-- STYLESHEET -->
    <link rel="stylesheet" href="/css/style.css?v=1.0.11">
    <link rel="stylesheet" href="/css/custom.css?v=1.0.11">
    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,500,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Cookie:400' rel='stylesheet' type='text/css'>
    <!-- FAVICON -->
    <!-- here is favicon links -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="home01">

@include('partials.header')

@yield('content')

@include('partials.footer')

<!--==============================
        Footer js pluging -->
<!-- jQuery -->
<script src="{{ asset('/js/jquery-1.12.0.min.js') }}"></script>
<!-- modernizr -->
<script src="{{ asset('/js/vendor/modernizr-2.8.3.min.js') }}"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
<!-- wow js-->
<script type="text/javascript" src="{{ asset('/js/wow.min.js') }}"></script>
<!-- venobox js-->
<script type="text/javascript" src="{{ asset('/js/venobox.min.js') }}"></script>
<!-- mouse hover js-->
<script src="{{ asset('/js/jquery.directional-hover.js') }}"></script>
<!-- owl js -->
<script src="{{ asset('/js/owl.carousel.min.js') }}"></script>
<!-- magnific popup -->
<script src="{{ asset('/js/jquery.magnific-popup.min.js') }}"></script>
<!-- settings -->
<script type="text/javascript" src="{{ asset('/js/setting.js') }}"></script>

<!--
<script type="text/javascript" src="{{ asset('/js/sweetalert.min.js') }}"></script>
-->

@stack('scripts')

@if (session('swal_status'))
    <script>
        swal({!! session('swal_status') !!});
    </script>
@endif

</body>
</html>
