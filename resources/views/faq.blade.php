@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="active"><a href="{{ url('faq') }}">FAQs</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->
            <div class="about01">
                <!-- about shop area -->
                <div class="about_our_shop_area">
                    <div class="row">
                        @each('components.faq-item', \App\Faq::all(), 'faq', 'components.empty-faq')
                    </div>
                </div>
                <!--/.about shop area-->
        </div>
        <!--/.container-->
        </div>
    </div>
    <!--/.about-content-->
@endsection
