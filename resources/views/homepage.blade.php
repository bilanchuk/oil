@extends('layouts.master')
@section('title', $page->seo_title)
@section('meta_keyword', $page->meta_keywords)
@section('meta_description', $page->meta_description)

@push ('scripts')
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="{{ asset('/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    <!-- fancybox -->
    <script src="{{ asset('/js/jquery.fancybox.min.js') }}"></script>
    <!-- settings -->
    <script type="text/javascript" src="{{ asset('/js/setting-revolution-1.js') }}"></script>

    @if ($errors->first('subscription_email'))
        <script>
            swal({
                title: "Subscription Error!",
                text: "{{ $errors->first('subscription_email') }}",
                icon: "error",
                buttons: ["Cancel", "Go to subscription from"],
            }).then(() => {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#subscription").offset().top
                }, 500);
            });
        </script>
    @endif
@endpush

@section('content')
<!--Site-Content-->
<div id="#content" class="site-content">
    <div class="container">
    @include('partials.slider')
    <!--Videos area-->
        <!--Featured Collection-->
        <div class="featured-collection">
            <div class="row">
                @foreach (\App\HomepageVideo::all() as $video)
                    <div class="video-block col-md-{{ $loop->iteration % 2 ? 8 : 4 }} col-sm-12">
                        <a href="{{ url($video->link)}}" class="single-featured left-featured video-thumbnail">
                            <img src="{{ Voyager::image($video->image) }}">
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="row">
                @each('components.banners', \App\Banner::all(), 'banner')
            </div>
            <!--/.row-->
        </div>
        <!--/.featured-collection-->
        <!--Latest items -->
        <div class="latest-items">
            <div class="tab-header">
                <div class="heading-title">
                    <h3 class="title-text">LATEST SOLUTIONS</h3>
                </div>
                <!--/.tab-item-->
            </div>
            <!--/.tab-header-->
            <div class="tab-content row">
                <div id="new" class="tab-pane fade in active">
                    @each('components.products-1', \App\Product::where('latest_solution', '1')->get(), 'latest_solution')
                </div>
            </div>
            <!--/.tab-content-->
        </div>
        <!--/.Latest items-->
        <!--Subscribe Area-->
        <!--/.subscribe area-->
        <!--/.Items-Carosel-Area-->
        <!--/.items-carosel-area-->
        <!--Latest Blogs-->
        <div class="latest-blogs">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h3 class="title-text">LATEST NEWS FROM THE BLOG</h3>
                    </div>
                    <!--/.heading-title-->
                </div>
                <!--/.col-md-12-->
                @foreach(\App\Article::all()->forPage(1, 3) as $post)
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <article class="post">
                        <div class="post-thumb-content">
                            <figure class="post-thumb">
                                <a href="{{ route('blogpost') }}"> <img class="img-responsive" alt="thumb" src="{{ Voyager::image($post->image) }}"> </a>
                            </figure>
                            <!--/.post thumb--><span class="entry-date">{{ $post->created_at }}</span>
                        </div>
                        <!--/.post-thumb-content-->
                        <div class="post-details">
                            <h3 class="entry-title">
                                <a href="{{ route('blogpost') }}">{{ $post->title }}</a>
                            </h3>
                            <!--/.entry title-->
                            <div class="entry-content">
                                {!! $post->excerpt !!}
                            </div>
                            <!--/.entry content-->
                        </div>
                        <!--/.post details-->
                    </article>
                    <!--/.post-->
                </div>
                <!--/.col-md-4-->
                @endforeach
            </div>
            <!--/.row-->
        </div>
        <!--/.latest blogs -->
        <!--content -->
        <div class="content">
            {!! $page->body !!}
        </div>
        <!-- /content -->
        @if (count(\App\Client::all()))
            <!--clients-->
            <div class="clients">
                <ul class="client-carousel">
                    @foreach (\App\Client::all() as $client)
                        <li>
                            <a href="javascript:void(0)" style="cursor: default"><img alt="clients logo" src="{{ Voyager::image($client->image) }}">
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <!--/.client -->
        @endif
    </div>
    <!--/.container-->
</div>
<!--/.site-content-->
@endsection
