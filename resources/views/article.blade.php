@extends('layouts.master')
@section('title', $article->seo_title)
@section('meta_keyword', $article->meta_keywords)
@section('meta_description', $article->meta_description)
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="active"><a href="{{ route('blogpost') }}">blog post</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->


            <div class="single-post-page">
                <div class="row">
                    <div class="col-md-9">
                        <article class="post">
                            <div class="post-thumb-content">
                                <figure class="post-thumb">
                                    <a href="#">
                                        <img class="img-responsive" alt="thumb" src="{{Voyager::image($article->image)}}">
                                    </a>
                                </figure>
                                <!--/.post thumb-->
                                <span class="entry-date">{{$article->created_at->day}}th {{date("M", mktime(0, 0, 0, $article->created_at->month, 1))}}. {{$article->created_at->year}}</span>
                            </div>
                            <!--/.post-thumb-content-->
                            <div class="post-details">
                                <h3 class="entry-title">
                                    {{$article->title}}
                                </h3>
                                <!--/.entry title-->
                                <div class="entry-content">
                                    {!! nl2br(e($article->body)) !!}
                                </div>
                                <!--/.entry content-->
                            </div>
                            <!--/.post details-->
                            <div class="share-post">
                                <span>Share</span>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-share-alt"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </div>
                            <!--/.author social-->
                        </article>
                        <!--/.post-->
                    </div>
                    <!--/.col-md-10-->
                </div>
                <!--/.row-->
            </div>
            <!--/.site-content-->
        </div>
        <!--/.container-->
    </div>
    <!--/.site-content-->
@endsection
