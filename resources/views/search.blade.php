<?php
/**
 * @var \App\Product $products
 */

if (!empty($_GET['s'])) {
    $products = \App\Product::whereRaw('LOWER(title) LIKE ?', '%' . strtolower($_GET['s']) . '%')
        ->orWhereRaw('LOWER(description) LIKE ?', '%' . strtolower($_GET['s']) . '%')
        ->get();
} else {
    $products = \App\Product::all();
}
?>
@extends('layouts.master')
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content shop-grid">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">{{ __('Home') }}</a>
                            </li>
                            <li class="active">
                                {{ __('Search') }}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->

            <div class="row">
                <div class="col-md-12">
                    <div class="product-listing-view">
                        @if (count($products))
                            <div class="view-navigation">
                                <div class="info-text">
                                    <p>{{ __(trans_choice('{1} Showing :num product|[1,*] Showing 1-:num from :num products', count($products)), ['num' => count($products)]) }}</p>
                                </div>
                                <div class="right-content">
                                    <div class="grid-list"></div>
                                    <div class="input-select"></div>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="shop-product-list">
                                @each('components.products-2', $products, 'product', 'components.empty-products')
                            </div>
                        </div>
                    </div>

                    {{--<!-- pagination -->
                    <div class="paginations text-center">
                        <ul class="pagination">
                            <li><a href="#"><span class="pagicon arrow_carrot-left"></span></a>
                            </li>
                            <li><a href="#"><span>1</span></a>
                            </li>
                            <li class="active"><a href="#"><span>2</span></a>
                            </li>
                            <li><a href="#"><span>3</span></a>
                            </li>
                            <li><a href="#"><span>4</span></a>
                            </li>
                            <li><a href="#"><span>5</span></a>
                            </li>
                            <li><a href="#"><span>6</span></a>
                            </li>
                            <li><a href="#"><span class="pagicon arrow_carrot-right"></span></a>
                            </li>
                        </ul>
                    </div>
                    <!--/.pagination-->
--}}
                </div>
            </div>
        </div>
        <!--/.container-->
    </div>
    <!--/.about-content-->
@endsection
