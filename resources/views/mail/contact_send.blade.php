
        <H2>Message from Everest.</H2>
    
        <table width="100%" border="0"  cellpadding="2" style="background-color: #E6E6E6">

            <tbody>
                @isset($feedback['name'])
                    <tr>
                        <td with="50">Name</td>
                        <td>{{ $feedback['name']}}</td>
                    </tr>
                @endisset
                @isset($feedback['phone'])
                    <tr>
                        <td with="50">Phone</td>
                        <td>{{ $feedback['phone']}}</td>
                    </tr>
                @endisset
                @isset($feedback['email'])
                    <tr>
                        <td with="50">Email</td>
                        <td>{{ $feedback['email']}}</td>
                    </tr>
                @endisset

                @isset($feedback['message'])
                  <tr>
                        <td with="50">Message</td>
                        <td>{{ $feedback['message'] }}</td>
                  </tr>      
                @endisset
            </tbody>
        </table>


