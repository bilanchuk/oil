<!--Slider-area-->
<div class="slider-area">
    <div class="container">
        <div class="tp-banner-container">
            <div class="tp-banner" id="tp-banner-1">
                <ul>
                    @foreach (\App\Slider::orderBy('order', 'asc')->get() as $slider)
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500">
                            <!-- MAIN IMAGE --><img src="{{ Voyager::image($slider->image) }}" alt="slidebg1" data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<!--/.slider-area-->
