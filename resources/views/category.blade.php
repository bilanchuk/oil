<?php
/**
 * @var \App\Category $category
 * @var \App\Product $products
 */
?>
@extends('layouts.master')
@section('title', $category->seo_title)
@section('meta_keyword', $category->meta_keywords)
@section('meta_description', $category->meta_description)
@section('content')
    <!--Site-Content-->
    <div id="#content" class="site-content shop-grid">
        <div class="container">
            <!--page title-->
            <div class="page_title_area row">
                <div class="col-md-12">
                    <div class="bredcrumb">
                        <ul>
                            <li><a href="{{ url('/') }}">{{ __('Home') }}</a>
                            </li>
                            <li class="active"><a
                                        href="{{ $category->link ?? url('/products') }}">{{ $category->title ?? __('All Products') }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/.page title-->

            <div class="row">
                <div class="col-md-12">
                    <div class="product-listing-view">
                        <div class="view-navigation">
                            <div class="info-text">
                                <?php $count=0;?>
                                @foreach ($category->products as $item)
                                @if ($item->active==1)
                                    <?php $count++; ?>
                                @endif
                                @endforeach
                                <p>Showing {{$count?1:0}} - {{$count}}</p>
                            </div>
                            <div class="right-content">
                                <div class="grid-list">
                                </div>
                                <div class="input-select">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="shop-product-list">
                                @each('components.products-2', $category->products ?? $products ?? [], 'product', 'components.empty-products')
                            </div>
                        </div>
                    </div>

                    {{--<!-- pagination -->
                    <div class="paginations text-center">
                        <ul class="pagination">
                            <li><a href="#"><span class="pagicon arrow_carrot-left"></span></a>
                            </li>
                            <li><a href="#"><span>1</span></a>
                            </li>
                            <li class="active"><a href="#"><span>2</span></a>
                            </li>
                            <li><a href="#"><span>3</span></a>
                            </li>
                            <li><a href="#"><span>4</span></a>
                            </li>
                            <li><a href="#"><span>5</span></a>
                            </li>
                            <li><a href="#"><span>6</span></a>
                            </li>
                            <li><a href="#"><span class="pagicon arrow_carrot-right"></span></a>
                            </li>
                        </ul>
                    </div>
                    <!--/.pagination-->
--}}
                </div>
            </div>
            <!--content -->
            <div class="content bottom-3">
                {!! $category->body !!}
            </div>
            <!-- /content -->
        </div>
        <!--/.container-->
    </div>
    <!--/.about-content-->
@endsection
