<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::get('/blogpost', function () {
    return view('blogpost');
})->name('blogpost');



Route::get('/single-product', function () {
    return view('single-product');
})->name('single-product');

Route::get('/search', function () {
    return view('search');
})->name('search');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::post('/subscribe', 'SubscriptionController@subscribe')->name('subscribe');
Route::post('/footer_contact', 'ContactAttemptController@footer')->name('contact_attempts.footer');
Route::post('/contact', 'ContactAttemptController@contact')->name('contact_attempts.contact');

Route::post('/order_marketing_materials', 'ContactAttemptController@order_marketing_materials')->name('contact_attempts.order_marketing_materials');

$pages = \App\Page::all();

foreach ($pages as $page) {
    $slug = $page->slug;
    Route::get($slug, function () use ($slug) {
        $page = \App\Page::whereSlug($slug)
            ->firstOrFail();
        switch ($slug){
            case '/':
                return view('homepage', compact('page'));
                break;
            case 'products':
                 $products = \App\Product::all();
                return view('products', compact('page', 'products'));
                break;
            case 'blog':
                return view('blog', compact('page'));
                break;
            case 'contacts':
                return view('contacts', compact('page'));
                break;
            case 'faq':
                return view('faq', compact('page'));
                break;
            case 'documentation':
                return view('documentation', compact('page'));
                break;
            case 'marketing_materials':
                $materials = \App\Material::all();
                return view('marketing_materials', compact('page', 'materials'));
                break;

            default: return view('page', compact('page'));
        }
        
    });
}

$articles = \App\Article::all();

foreach ($articles as $article) {
    Route::get($article->slug, function () {
        $slug = request()->segment(1);
        $article = \App\Article::whereSlug($slug)
            ->firstOrFail();

        return view('article', compact('article'));
    });
}

$products = \App\Product::all();

foreach ($products as $product) {
    Route::get('product/' . $product->slug, function () {
        $slug = request()->segment(2);
        $product = \App\Product::whereSlug($slug)
            ->firstOrFail();

        return view('single-product', compact('product'));
    });
}

$categories = \App\Category::all();

foreach ($categories as $category) {
    Route::get('products/category/' . $category->slug, function () {
        $slug = request()->segment(3);
        $category = \App\Category::whereSlug($slug)
            ->firstOrFail();

        return view('category', compact('category'));
    });
}
