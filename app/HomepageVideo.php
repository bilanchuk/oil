<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\HomepageVideo
 *
 * @property int $id
 * @property int $order
 * @property string $title
 * @property string|null $link
 * @property string|null $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\HomepageVideo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\HomepageVideo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\HomepageVideo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\HomepageVideo whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\HomepageVideo whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\HomepageVideo whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\HomepageVideo whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\HomepageVideo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class HomepageVideo extends Model
{
    //
}
