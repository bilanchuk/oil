<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Product
 *
 * @property int $id
 * @property int $order
 * @property string $title
 * @property string|null $seo_title
 * @property string|null $product_id
 * @property string|null $excerpt
 * @property string|null $description
 * @property string|null $image
 * @property mixed|null $images
 * @property mixed|null $files
 * @property string $slug
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property int $top_seller
 * @property int $latest_items
 * @property int $popular_products
 * @property int $latest_solution
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $all_images
 * @property-read mixed $link
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Packaging[] $packagings
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Product onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereFiles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereLatestItems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereLatestSolution($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product wherePopularProducts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereSeoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereTopSeller($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Product withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property string|null $comparison_criteria
 * @property int|null $comparison_1_enabled
 * @property int|null $comparison_2_enabled
 * @property int|null $comparison_3_enabled
 * @property string|null $comparison_1_image
 * @property string|null $comparison_2_image
 * @property string|null $comparison_3_image
 * @property string|null $comparison_1_title
 * @property string|null $comparison_2_title
 * @property string|null $comparison_3_title
 * @property string|null $comparison_0_values
 * @property string|null $comparison_1_values
 * @property string|null $comparison_2_values
 * @property string|null $comparison_3_values
 * @property-read mixed $show_comparison_table
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison0Values($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison1Enabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison1Image($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison1Title($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison1Values($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison2Enabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison2Image($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison2Title($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison2Values($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison3Enabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison3Image($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison3Title($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparison3Values($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereComparisonCriteria($value)
 */
class Product extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function getLinkAttribute()
    {
        return url('/product/' . $this->slug);
    }

    public function getAllImagesAttribute()
    {
        $images = !empty($this->image) ? [$this->image] : [];
        if (count((array) json_decode($this->images))) {
            $images = !empty($images) ? array_merge($images, json_decode($this->images)) : json_decode($this->images);
        }

        return $images;
    }

    public function packagings()
    {
        return $this->belongsToMany(Packaging::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function getShowComparisonTableAttribute()
    {
        return $this->comparison_1_enabled || $this->comparison_2_enabled || $this->comparison_3_enabled;
    }

    public function getComparisonTitlesAttribute()
    {
        $titles = [];
        $titles[] = $this->title;
        if ($this->comparison_1_enabled)
            $titles[] = $this->comparison_1_title;
        if ($this->comparison_2_enabled)
            $titles[] = $this->comparison_2_title;
        if ($this->comparison_3_enabled)
            $titles[] = $this->comparison_3_title;

        return $titles;
    }

    public function getComparisonImagesAttribute()
    {
        $images = [];
        $images[] = $this->image;
        if ($this->comparison_1_enabled)
            $images[] = $this->comparison_1_image;
        if ($this->comparison_2_enabled)
            $images[] = $this->comparison_2_image;
        if ($this->comparison_3_enabled)
            $images[] = $this->comparison_3_image;

        return $images;
    }

    public function getComparisonRowsAttribute()
    {
        $rows = [];
        $criteria = $this->splitTextarea($this->comparison_criteria) ?? [];
        foreach ($criteria as $key => $criterion) {
            $row = ['title' => $criterion];
                $row['values'][] = $this->splitTextarea($this->comparison_0_values)[$key] ?? '';
            if ($this->comparison_1_enabled)
                $row['values'][] = $this->splitTextarea($this->comparison_1_values)[$key] ?? '';
            if ($this->comparison_2_enabled)
                $row['values'][] = $this->splitTextarea($this->comparison_2_values)[$key] ?? '';
            if ($this->comparison_3_enabled)
                $row['values'][] = $this->splitTextarea($this->comparison_3_values)[$key] ?? '';

            $rows[] = $row;
        }

        return $rows;
    }

    private function splitTextarea($value){
        return preg_split('/\r\n|\r|\n/', $value);
    }
}
