<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function subscribe(Request $request)
    {
        $request->validate([
            'subscription_email' => 'required|max:255|email',
        ]);

        try {
            $subscription = new Subscription();
            $subscription->email = $request->input('subscription_email');
            $subscription->save();
        } catch (\Exception $e) {
            return \Redirect::back()->with('swal_status', json_encode([
                'title' => 'Subscription failed.',
                'icon' => 'error',
            ]));
        }

        return \Redirect::back()->with('swal_status', json_encode([
            'title' => 'All saved.',
            'text' => 'Thank you for the subscription!',
            'icon' => 'success',
        ]));
    }
}
