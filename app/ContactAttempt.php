<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ContactAttempt
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactAttempt whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactAttempt whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactAttempt whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactAttempt whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactAttempt whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactAttempt wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ContactAttempt whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactAttempt extends Model
{
    //
}
